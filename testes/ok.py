from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GLU import *
import sys

def main():
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH)
    glutInitWindowSize(600, 600)
    glutCreateWindow('Teste Bolinha')

    glClearColor(0.,0.,0.,1.5)
    glShadeModel(GL_SMOOTH)
    glEnable(GL_CULL_FACE)
    glEnable(GL_DEPTH_TEST)
    glEnable(GL_LIGHTING)
    lightZeroPosition = [10.,4.,10.,1.]
    lightZeroColor = [0.8,1.0,0.8,1.0]
    glLightfv(GL_LIGHT0, GL_POSITION, lightZeroPosition)
    glLightfv(GL_LIGHT0, GL_DIFFUSE, lightZeroColor)
    glLightf(GL_LIGHT0, GL_CONSTANT_ATTENUATION, 0.1)
    glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, 0.05)
    glEnable(GL_LIGHT0)
    glutDisplayFunc(display)
    glutMouseFunc(ArrastaPrimitiva)
    glMatrixMode(GL_PROJECTION)
    gluPerspective(40.,1.,1.,40.)
    glMatrixMode(GL_MODELVIEW)
    gluLookAt(0,0,10,
              0,0,0,
              0,1,0)
    glPushMatrix()
    glutMainLoop()
    return

def display():
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
    glPushMatrix()
    color = [0.0,0.6666666666666666,1.0,1.0]
    glMaterialfv(GL_FRONT,GL_DIFFUSE,color)
    glutSolidSphere(2,40,20)
    # glutSolidTeapot(2)
    glPopMatrix()
    glutSwapBuffers()
    return

def ArrastaPrimitiva(botao, input, x, y):

	if botao == 0:
		txt = 'Mouse 1'

	elif botao == 1:
		txt = 'Mouse scroll'

	elif botao == 2:
    # else:
	    txt = 'Mouse 2'

	if input:
		input = 'Soltou'
		x2 = y

	else:
		input = 'Apertou'
		x1 = x

    print('Botao pressionado: \033[1m{0}\033[0m\nInput: {1}\nPonto: ({2}, {3})'.format(txt, input, x, y))

    if input == 'Soltou':

        print('dX2-dX1 = {0}'.format(x2-x1))

	glutPostRedisplay()

if __name__ == '__main__':
    main()
