######################################################
# ========== Computação Gráfica (CC-6111) ========== #
# == Fernando Mainetti Secol (22.213.031-2)       == #
# == Atividade II LAB (26/08/2016)                == #
######################################################

from OpenGL.GL     import *
from OpenGL.GLUT   import *
from OpenGL.GLU    import *
from OpenGL._bytes import as_8_bit
import sys

# Mapa de valores teclado

ESC = as_8_bit('\033')
K_1 = as_8_bit('\061')
K_2 = as_8_bit('\062')
K_3 = as_8_bit('\063')
K_Z = as_8_bit('\172')
K_X = as_8_bit('\170')
K_C = as_8_bit('\143')
K_V = as_8_bit('\166')
K_M = as_8_bit('\155')
K_N = as_8_bit('\156')
x1  = 0
x2  = 0

# Draw Circunferência

def display_bola():
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
	glPushMatrix()
	glutSolidSphere(2,40,20)
	glPopMatrix()
	glutSwapBuffers()
	return

# Draw Bule

def display_bule():
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
	glPushMatrix()
	glutSolidTeapot(2)
	glPopMatrix()
	glutSwapBuffers()
	return

# Draw Cone

def display_cone():
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
	glPushMatrix()
	glRotatef(250, 1, 0, 0)
	glutSolidCone(1, 2, 50, 10)
	glPopMatrix()
	glutSwapBuffers()
	return

# Se tecla apertada
# Obs. Python não possui switch case

def tecla_apertada(*args):
	if args[0] == ESC:
		sys.exit()
	elif args[0] == K_1:
		print('Teclado 1')
		glutDisplayFunc(display_bola)
		glutPostRedisplay()
	elif args[0] == K_2:
		print('Teclado 2')
		glutDisplayFunc(display_bule)
		glutPostRedisplay()
	elif args[0] == K_3:
		print('Teclado 3')
		glutDisplayFunc(display_cone)
		glutPostRedisplay()
	elif args[0] == K_Z:
		print('Teclado Z')
		vermelho = [1.0,0.0,0.0,1.0]
		glColor(vermelho)
		glutPostRedisplay()
	elif args[0] == K_X:
		print('Teclado X')
		verde = [0.3333333333333333,0.6666666666666666,0.0,1.0]
		glColor(verde)
		glutPostRedisplay()
	elif args[0] == K_C:
		print('Teclado C')
		amarelo = [1.0,0.6666666666666666,0.0,1.0]
		glColor(amarelo)
		glutPostRedisplay()
	elif args[0] == K_V:
		print('Teclado V')
		azul = [0.0,0.3333333333333333,1.0,1.0]
		glColor(azul)
		glutPostRedisplay()

	elif args[0] == K_M:
		print('Teclado V')
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)
		glutPostRedisplay()

	elif args[0] == K_N:
		print('Teclado V')
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
		glutPostRedisplay()

# Func listener para callback de mouse

def ArrastaPrimitiva(botao, input, x, y):

	if botao == 0:
		txt = 'Mouse 1'

	elif botao == 1:
		txt = 'Mouse scroll'

	else:
		txt = 'Mouse 2'
	
	if input:
		input = 'Soltou'
		x2 = x

	else:
		input = 'Apertou'
		x1 = x

	print('Botao pressionado: \033[1m{0}\033[0m\nInput: {1}\nPonto: ({2}, {3})'.format(txt, input, x, y))

# Menu

def menu():

	menu_formato = glutCreateMenu(menu_opc)
 
	glutAddMenuEntry("Fill", 1)
	glutAddMenuEntry("Wire", 2)
 
	menu_cores = glutCreateMenu(menu_opc)

	glutAddMenuEntry("Vermelho", 3)
	glutAddMenuEntry("Verde", 4)
	glutAddMenuEntry("Amarelo", 5)
	glutAddMenuEntry("Azul", 6)
 
	menu_formas = glutCreateMenu(menu_opc)

	glutAddMenuEntry("Circunferência", 7)
	glutAddMenuEntry("Bule", 8)
	glutAddMenuEntry("Cone", 9)

	menu_id = glutCreateMenu(menu_opc)
 
	glutAddSubMenu("Tipo", menu_formato)
	glutAddSubMenu("Cor", menu_cores)
	glutAddSubMenu("Forma", menu_formas)

	glutAddMenuEntry("Sair", 0)
 
	glutAttachMenu(GLUT_RIGHT_BUTTON)

# Handler das opções do menu

def menu_opc(valor):
	if valor == 1:
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
	elif valor == 2:
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)
	elif valor == 3:
		vermelho = [1.0,0.0,0.0,1.0]
		glColor(vermelho)
	elif valor == 4:
		verde = [0.3333333333333333,0.6666666666666666,0.0,1.0]
		glColor(verde)
	elif valor == 5:
		amarelo = [1.0,0.6666666666666666,0.0,1.0]
		glColor(amarelo)
	elif valor == 6:
		azul = [0.0,0.3333333333333333,1.0,1.0]
		glColor(azul)
	elif valor == 7:
		glutDisplayFunc(display_bola)
	elif valor == 8:
		glutDisplayFunc(display_bule)
	elif valor == 9:
		glutDisplayFunc(display_cone)
	elif valor == 0:
		sys.exit()
	else:
		sys.exit()
 
	glutPostRedisplay()

# Main principal

def main():

	glutInit(sys.argv)
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH)
	glutInitWindowSize(600, 600)
	glutCreateWindow('Lab 1')
	glClearColor(0.0, 0.0, 0.0, 1.0)
	glutDisplayFunc(display_bola)
	glutKeyboardFunc(tecla_apertada)
	gluPerspective(40.,1.,1.,40.)
	glMatrixMode(GL_MODELVIEW)

	cor = [1.0,0.0,0.0,1.0]

	glColor(cor)
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)

	gluLookAt(0,0,10,
			  0,0,0,
			  0,1,0)

	glPushMatrix()
	menu()
	glutMouseFunc(ArrastaPrimitiva)
	glutMainLoop()

if __name__ == '__main__':
	main()